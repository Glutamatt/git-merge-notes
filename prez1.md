first prez : make big projects happy
google guy, gerrit, git team

git at google since 2008

android :
800 repos
17Go for AOSP tree
permissions for partners
tools are opened

Chrome
chrome + chromeOs + Blink
migrated from svn
merge chromium and Blink

issues with big repos for client
download time
checking out large file set
change habits (don't do that)

solutions : 
Large file system for big files
clones shallow/narrow
split into different repo : python wraper
submodules, don't handle recursive actions well

issues with big repos for servers
counting objects : Linux 100%CPU for one minute

solutions:
bundles : redirect to cdn + some incrementals fetch (reduced server load, resumable)
nfs, masterslave, replication

issues with big repos for Humans
too much repositories
too much users => too much reviews
handle forks

ACL with gerrit
code review in any way
